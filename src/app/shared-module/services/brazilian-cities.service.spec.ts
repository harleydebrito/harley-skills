import { TestBed } from '@angular/core/testing';

import { BrazilianCitiesService } from './brazilian-cities.service';

describe('BrazilianCitiesService', () => {
  let service: BrazilianCitiesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BrazilianCitiesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
