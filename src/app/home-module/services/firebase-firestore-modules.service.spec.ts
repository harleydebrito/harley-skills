import { TestBed } from '@angular/core/testing';

import { FirebaseFirestoreModulesService } from './firebase-firestore-modules.service';

describe('FirebaseFirestoreModulesService', () => {
  let service: FirebaseFirestoreModulesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FirebaseFirestoreModulesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
