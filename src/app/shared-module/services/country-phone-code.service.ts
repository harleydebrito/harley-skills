import { Country } from './../models/country';
import { take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class CountryPhoneCodeService {

  constructor(private http: HttpClient) { }

  public getCountryPhoneCodes(): Observable<Country[]> {
    return this.http.get<Country[]>('assets/json/country_phone_code.json').pipe(take(1));
  }
}
