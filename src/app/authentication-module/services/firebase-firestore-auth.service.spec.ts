import { TestBed } from '@angular/core/testing';

import { FirebaseFirestoreAuthService } from './firebase-firestore-auth.service';

describe('FirebaseFirestoreAuthService', () => {
  let service: FirebaseFirestoreAuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FirebaseFirestoreAuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
