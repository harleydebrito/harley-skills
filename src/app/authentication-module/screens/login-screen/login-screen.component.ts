import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { EMPTY, Subscription } from 'rxjs';
import { switchMap, take } from 'rxjs/operators';

import { ModalService } from './../../../shared-module/services/modal.service';
import { FirebaseAuthService } from '../../services/firebase-auth.service';
import { BaseFormComponent } from './../../../shared-module/components/base-form/base-form.component';
import { AuthenticationErrorMsgService } from '../../services/authentication-error-msg.service';

@Component({
  selector: 'app-login-screen',
  templateUrl: './login-screen.component.html',
  styleUrls: ['./login-screen.component.scss']
})

export class LoginScreenComponent extends BaseFormComponent implements OnInit, OnDestroy {
  public waitingForEmailVerification: boolean = false;
  public emailToVerify: string;
  public firebaseAuthService$: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    public firebaseAuthService: FirebaseAuthService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private modalService: ModalService,
    private authenticationErrorMsgService: AuthenticationErrorMsgService,
  ) {
    super();
  }
  
  public ngOnInit(): void {
    this.wasRedirected();
    this.buildLoginForm();
    this.listenToFirebaseAuthService();
  }

  public ngOnDestroy(): void {
    this.firebaseAuthService$.unsubscribe();
  }

  private wasRedirected(): void {
    this.activatedRoute.queryParams
      .pipe(
        take(1),
        switchMap((params: Params) => params['redirect'] ? this.firebaseAuthService.firebaseAuth.user : EMPTY),
        take(1))
      .subscribe((user: firebase.default.User) => {
        if(user){
          this.firebaseAuthService.firebaseAuth.signOut();
          this.emailToVerify = user.email;
          this.waitingForEmailVerification = true;
        }
      });
  }

  private buildLoginForm(): void {
    this.form = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]]
    });
  }

  private listenToFirebaseAuthService(): void {
    this.firebaseAuthService$ = this.firebaseAuthService.subject.asObservable()
      .subscribe(
        response => this.firebaseAuthServiceResponse(response)
      );
  }

  private firebaseAuthServiceResponse(res: any): void {
    if(res.success) {
      this.firebaseAuthSuccess(res.content);
    }else{
      this.firebaseAuthFailure(res.content);
    }
  }

  private firebaseAuthSuccess(user: firebase.default.User): void {
    if (!user.emailVerified) {
      this.firebaseAuthService.logout();
      this.waitingForEmailVerification = true;
      this.emailToVerify = user.email;
      this.form.reset();
    } else {
      this.router.navigate(['home']);
    }
  }

  private firebaseAuthFailure(code: number): void {
    this.modalService.showDangerAlert(this.authenticationErrorMsgService.getErrorMsg(code));
  }

  public submit(): void {
    this.waitingForEmailVerification = false;
    this.signIn();
  }

  public signIn(): void {
    this.firebaseAuthService.signIn(this.form.value);
  }
}
