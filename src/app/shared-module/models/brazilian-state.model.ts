import { EMPTY } from "rxjs";

export class BrazilianState {
    private _id: string;
    private _initials: string;
    private _name: string;
    constructor(
        id?: string,
        initials?: string,
        name?: string
    ){
        id ? this._id = id : EMPTY;
        initials ? this._initials = initials : EMPTY;
        name ? this._name = name : EMPTY;
    }
    public set id(id: string) {
        this._id = id;
    }
    public set initials(initials: string) {
        this._initials = initials;
    }
    public set name(name: string) {
        this._name = name;
    }
    public get id(): string {
        return this._id;
    }
    public get initials(): string {
        return this._initials;
    }
    public get name(): string {
        return this._name;
    }
}