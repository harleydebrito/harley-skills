import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScreenCenteredContainerComponent } from './screen-centered-container.component';

describe('ScreenCenteredContainerComponent', () => {
  let component: ScreenCenteredContainerComponent;
  let fixture: ComponentFixture<ScreenCenteredContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScreenCenteredContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScreenCenteredContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
