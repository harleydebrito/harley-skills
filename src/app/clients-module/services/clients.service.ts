import { Collections, CounterOperation, CounterService } from './../../shared-module/services/counter.service';
import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';

import { FirestoreErrorMsgService } from './../../shared-module/services/firestore-error-msg.service';

export interface Address {
  cep: string;
  city: string;
  complement?: string;
  country: string;
  number: string;
  state: string;
  street: string;
  district: string;
}

export interface Client {
  id?: string;
  name: string;
  cpf: string;
  phone?: string;
  rg?: string;
  address: Address;
  deleted: boolean;
  photo?: string | boolean;
}

export interface ClientsServiceResponse {
  result?: boolean;
  payload?: DocumentReference<Client> | Client[] | firebase.default.storage.UploadTaskSnapshot | Error | any;
  message?: string;
  count?: number;
}

@Injectable({
  providedIn: 'root'
})

export class ClientsService {
  public loading: boolean = false;
  public loaded: boolean = false;
  public saving: boolean = false;
  public deleting: boolean = false;

  constructor(
    private angularFirestore: AngularFirestore,
    private angularFireStorage: AngularFireStorage,
    private counterService: CounterService
  ) {  }

  public get busy() {
    return this.loading || this.saving || this.deleting;
  }

  public async save(client: Client): Promise<ClientsServiceResponse> {
    this.saving = true;
    let saved: DocumentReference<Client> | void;
    let count: number;
    let message: string;
    let photo: string | boolean;
    try {
      if (typeof client.photo != "boolean") {
        photo = (client.photo as string)?.split('base64,')[1];
        client.photo = null;
        photo ? (client.photo as boolean) = true : (client.photo as boolean) = false;
      }
      if (client.id) {
        saved = await this.angularFirestore.collection<Client>('clients').doc(client.id).update(client) as void;
        if (photo) {
          await this.angularFireStorage.ref('clients/profilePhotos/').child(`${client.id}`).putString(photo, 'base64', { contentType: 'image/jpg' });
        }
        count = await this.counterService.getCount(Collections.CLIENTS);
        message = `Cliente ${client.name} atualizado(a) com sucesso.`;
      } else {
        saved = await this.angularFirestore.collection<Client>('clients').add(client) as DocumentReference<Client>;
        if (photo) {
          await this.angularFireStorage.ref('clients/profilePhotos/').child(`${saved.id}`).putString(photo, 'base64', { contentType: 'image/jpg' });
        }
        count = await this.counterService.change(Collections.CLIENTS, CounterOperation.INCREMENT);
        message = `Cliente ${client.name} criado(a) com sucesso.`;
      }
      return this.stopLoadingAndComplete({ operation: 'save', payload: saved, message, count });
    } catch (error) {
      console.log(error);
      return this.stopLoadingAndComplete({ result: false, operation: 'save', payload: error });
    }
  }

  public async load({ size = 5, last, first, name }: { size?: number, last?: Client, first?: Client, name?: string }): Promise<ClientsServiceResponse> {
    this.loading = true;
    let response: any;
    try {
      if (name) {
        response = await this.angularFirestore.collection<Client>('clients', ref => ref.orderBy('name', 'asc').where('deleted', '==', false).startAt(name).endAt(`${name}\uf8ff`)).get().toPromise();
      } else {
        if (first) {
          response = await this.angularFirestore.collection<Client>('clients', ref => ref.orderBy('name', 'asc').where('deleted', '==', false).endBefore(first.name).limitToLast(size)).get().toPromise();
        } else if (last) {
          response = await this.angularFirestore.collection<Client>('clients', ref => ref.orderBy('name', 'asc').where('deleted', '==', false).startAfter(last.name).limit(size)).get().toPromise();
        } else {
          response = await this.angularFirestore.collection<Client>('clients', ref => ref.orderBy('name', 'asc').where('deleted', '==', false).limit(size)).get().toPromise();
        }
      }
      
      let clientsReturn: Client[] = new Array<Client>();
      for (let document of response.docs) {
        let client: Client = document.data();
        client.id = document.id;
        if (client.photo){
          client.photo = await this.angularFireStorage.storage.ref().child(`clients/profilePhotos/${client.id}`).getDownloadURL();
        }
        clientsReturn.push(client);
      }
      let count = await this.counterService.getCount(Collections.CLIENTS);
      return this.stopLoadingAndComplete({ operation: 'load', payload: clientsReturn, count });
    } catch (error) {
      console.log(error);
      return this.stopLoadingAndComplete({ result: false, operation: 'load', payload: error });
    }
  }

  public async delete(id: string): Promise<ClientsServiceResponse> {
    this.deleting = true;
    try {
      await this.angularFirestore.collection('clients').doc(id).update({ deleted: true });
      let count = await this.counterService.change(Collections.CLIENTS, CounterOperation.DECREMENT);
      return this.stopLoadingAndComplete({ operation: 'delete', message: 'Cliente deletado com sucesso', count });
    } catch (error) {
      return this.stopLoadingAndComplete({ operation: 'delete', payload: error, result: false });
    }
  }

  private stopLoadingAndComplete({ operation, payload, message, result = true, count }:{ operation: string, payload?: any, message?: string, result?: boolean, count?: number }): ClientsServiceResponse | Promise<ClientsServiceResponse>{
    this.stopLoading(operation);
    if (result){
      return { result, payload, message, count };
    } else {
      return Promise.reject({ result, payload, message: FirestoreErrorMsgService.getMessage(payload.code) });
    }
  }

  private stopLoading(operation: string): void{
    switch (operation) {
      case 'save':
        this.saving = false;
        break;
      case 'load':
        this.loaded = true;
        this.loading = false;
        break;
      case 'delete':
        this.deleting = false;
        break;
    }
  }
}
