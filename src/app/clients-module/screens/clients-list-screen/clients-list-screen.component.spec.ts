import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientsListScreenComponent } from './clients-list-screen.component';

describe('ClientsListScreenComponent', () => {
  let component: ClientsListScreenComponent;
  let fixture: ComponentFixture<ClientsListScreenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClientsListScreenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientsListScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
