import { NgxMaskModule } from 'ngx-mask';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxLoadingModule } from 'ngx-loading';
import { SharedModule } from './../shared-module/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientsRoutingModule } from './clients-routing.module';
import { ClientsScreenComponent } from './screens/clients-screen/clients-screen.component';
import { ClientFormScreenComponent } from './screens/client-form-screen/client-form-screen.component';
import { ClientsListScreenComponent } from './screens/clients-list-screen/clients-list-screen.component';
import { NgxShimmerLoadingModule } from 'ngx-shimmer-loading';
import { ClientFormModalComponent } from './modals/client-form-modal/client-form-modal.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    ClientsScreenComponent,
    ClientFormScreenComponent,
    ClientsListScreenComponent,
    ClientFormModalComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    NgxShimmerLoadingModule,
    NgbModule,
    NgxMaskModule,
    NgxLoadingModule,
    ImageCropperModule,
    ClientsRoutingModule
  ]
})

export class ClientsModule { }
