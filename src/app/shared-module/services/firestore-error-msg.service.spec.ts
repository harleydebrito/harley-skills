import { TestBed } from '@angular/core/testing';

import { FirestoreErrorMsgService } from './firestore-error-msg.service';

describe('FirestoreErrorMsgService', () => {
  let service: FirestoreErrorMsgService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FirestoreErrorMsgService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
