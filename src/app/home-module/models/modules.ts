import { EMPTY } from 'rxjs';
import { Link } from './links';

export class Module {
    private _title: string;
    private _description: string;
    private _actionLabel: string;
    private _links: Link[];

    constructor(title?: string, description?: string, actionLabel?: string, links?: Link[]){
        title ? this._title = title : EMPTY
        description ? this._description = description : EMPTY
        actionLabel ? this._actionLabel = actionLabel : EMPTY
        links ? this._links = links : EMPTY
    }

    public set title(title: string) {
        this._title = title;
    } 

    public set description(description: string) {
        this._description = description;
    }

    public set actionLabel(actionLabel: string) {
        this._actionLabel = actionLabel;
    }

    public set links(links: Link[]) {
        this._links = links;
    }

    public get title(): string {
        return this._title;
    }

    public get description(): string {
        return this._description;
    }

    public get actionLabel(): string {
        return this._actionLabel;
    }

    public get links(): Link[] {
        return this._links;
    }
}