export enum AuthenticationErrorMsg {
    USER_NOT_FOUND = 'Usuário não encontrado. Por favor, verifique seu email ou cadastre-se.',
    WRONG_PASSWORD = 'Senha incorreta. Por favor, tente novamente.',
    NETWORK_PROBLEM = 'Não foi possível entrar. Verifique sua conexão.',
    TOO_MANY_REQUESTS = 'O servidor detectou requisições incomuns, aguarde alguns segundos e tente novamente.',
    EMAIL_ALREADY_IN_USE = 'O email informado já está cadastrado.',
    DEFAULT = 'Contate o suporte.',
}