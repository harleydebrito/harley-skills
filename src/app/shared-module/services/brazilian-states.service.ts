import { map } from 'rxjs/operators';
import { BrazilianState } from './../models/brazilian-state.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class BrazilianStatesService {

  constructor(private http: HttpClient) { }

  public async getStates(): Promise<BrazilianState[]> {
    return this.http.get<BrazilianState[]>('assets/json/brazilian-states.json').toPromise();
  }

  public async getByInitial(initials: string): Promise<BrazilianState> {
    return this.http.get<BrazilianState[]>('assets/json/brazilian-states.json')
      .pipe(
        map((states: BrazilianState[]) => states.find((state: BrazilianState) => state.initials == initials))
      ).toPromise();
  }
}
