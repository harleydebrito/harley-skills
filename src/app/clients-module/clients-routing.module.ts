import { ClientFormScreenComponent } from './screens/client-form-screen/client-form-screen.component';
import { ClientsScreenComponent } from './screens/clients-screen/clients-screen.component';
import { ClientsListScreenComponent } from './screens/clients-list-screen/clients-list-screen.component';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', component: ClientsScreenComponent, children: [
    { path: '', component: ClientsListScreenComponent },
    { path: 'new',  component: ClientFormScreenComponent }
  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ClientsRoutingModule { }
