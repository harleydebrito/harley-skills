import { AppUrlService } from '../../../shared-module/services/app-url.service';

import { Subscription, Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clients-screen',
  templateUrl: './clients-screen.component.html',
  styleUrls: ['./clients-screen.component.scss']
})

export class ClientsScreenComponent implements OnInit{
  private routerEvent: Subscription;
  public routes$: Observable<string[]>;

  constructor(
    private appUrlService: AppUrlService
  ) { }

  public ngOnInit(): void {
    this.routerEvent = this.appUrlService.listenToRouterEvents();
    this.routes$ = this.appUrlService.routes;
  }

  public ngOnDestroy(): void {
    this.routerEvent.unsubscribe();
  }
}
