import { BrazilianCity } from './../models/brazilian-city.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class BrazilianCitiesService {

  constructor(private http: HttpClient) { }

  public async getCities(stateId: string): Promise<BrazilianCity[]> {
    return this.http.get<BrazilianCity[]>('assets/json/brazilian-cities.json')
      .pipe(
        // tslint:disable-next-line:triple-equals
        map((cities: BrazilianCity[]) => cities.filter(city => city.stateId == stateId))
      ).toPromise();
  }
}
