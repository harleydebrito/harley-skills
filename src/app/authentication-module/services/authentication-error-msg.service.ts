import { AuthenticationErrorMsg } from '../utils/error-msg.enum';

export class AuthenticationErrorMsgService {
  constructor(){}

  public getErrorMsg(code): string {
    switch(code){
      case 'auth/user-not-found': {
        return AuthenticationErrorMsg.USER_NOT_FOUND;
      }

      case 'auth/wrong-password': {
        return AuthenticationErrorMsg.WRONG_PASSWORD;
      }

      case 'auth/network-request-failed': {
        return AuthenticationErrorMsg.NETWORK_PROBLEM;
      }

      case 'auth/too-many-requests': {
        return AuthenticationErrorMsg.TOO_MANY_REQUESTS;
      }

      case 'auth/email-already-in-use': {
        return AuthenticationErrorMsg.EMAIL_ALREADY_IN_USE;
      }

      default: {
        return AuthenticationErrorMsg.DEFAULT;
      }
    }
  }
}
