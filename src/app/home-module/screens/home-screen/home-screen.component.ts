import { FirebaseFirestoreModulesService } from './../../services/firebase-firestore-modules.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-screen',
  templateUrl: './home-screen.component.html',
  styleUrls: ['./home-screen.component.scss'],
  preserveWhitespaces: true
})

export class HomeScreenComponent implements OnInit {
  private tick: boolean = false;
 
  constructor() { }

  public ngOnInit(): void { }

  public close(): void {
    this.tick = !this.tick;
  }
}
