import { FirestoreErrorMsg } from './../utils/firestore-error-msg.enum';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class FirestoreErrorMsgService {
  public static getMessage(code: string): string {
    switch (code) {
      case "failed-precondition":
        return FirestoreErrorMsg.FAILED_PRECONDITION;
      case "already-exists":
        return FirestoreErrorMsg.ALREADY_EXISTS;
      case "deadline-exceeded":
        return FirestoreErrorMsg.DEADLINE_EXCEEDED;
      case "internal":
        return FirestoreErrorMsg.INTERNAL;
      case "invalid-argument":
        return FirestoreErrorMsg.INVALID_ARGUMENT;
      case "not-found":
        return FirestoreErrorMsg.NOT_FOUND;
      case "permission-denied":
        return FirestoreErrorMsg.PERMISSION_DENIED;
      case "resource-exhausted":
        return FirestoreErrorMsg.RESOURCE_EXHAUSTED;
      case "unauthenticated":
        return FirestoreErrorMsg.UNAUTHENTICATED;
      case "unavailable":
        return FirestoreErrorMsg.UNAVAILABLE;
      default:
        return FirestoreErrorMsg.DEFAULT;
    }
  }
}
