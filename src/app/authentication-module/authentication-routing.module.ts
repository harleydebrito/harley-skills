import { RegisterScreenComponent } from './screens/register-screen/register-screen.component';
import { LoginScreenComponent } from './screens/login-screen/login-screen.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', component: LoginScreenComponent },
  { path: 'new', component: RegisterScreenComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AuthenticationRoutingModule { }
