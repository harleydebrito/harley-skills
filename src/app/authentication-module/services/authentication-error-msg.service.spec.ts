import { TestBed } from '@angular/core/testing';

import { AuthenticationErrorMsgService } from './authentication-error-msg.service';

describe('AuthenticationErrorMsgService', () => {
  let service: AuthenticationErrorMsgService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthenticationErrorMsgService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
