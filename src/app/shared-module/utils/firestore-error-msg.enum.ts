export enum FirestoreErrorMsg {
    FAILED_PRECONDITION = "Uma solicitação de consulta pode exigir um índice ainda não definido.",
    ALREADY_EXISTS = "A solicitação tentou criar um documento já existente.",
    DEADLINE_EXCEEDED = "O servidor do Cloud Firestore responsável pela solicitação ultrapassou o prazo.",
    INTERNAL = "O servidor do Cloud Firestore retornou um erro.",
    INVALID_ARGUMENT = "Um parâmetro de solicitação inclui um valor inválido.",
    NOT_FOUND = "A solicitação tentou atualizar um documento que não existe.",
    PERMISSION_DENIED = "O usuário não está autorizado a fazer essa solicitação.",
    RESOURCE_EXHAUSTED = "O projeto ultrapassou a cota ou a capacidade de região/multirregião.",
    UNAUTHENTICATED = "A solicitação não incluiu credenciais de autenticação válidas.",
    UNAVAILABLE = "O servidor do Cloud Firestore retornou um erro.",
    DEFAULT = "Erro não mapeado",
}