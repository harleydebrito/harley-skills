import { EMPTY } from "rxjs";

export class BrazilianCity {
    private _id: string;
    private _name: string;
    private _stateId: string;
    constructor(
        id?: string,
        stateId?: string,
        name?: string
    ){
        id ? this._id = id : EMPTY;
        stateId ? this._stateId = stateId : EMPTY;
        name ? this._name = name : EMPTY;
    }
    public set id(id: string) {
        this._id = id;
    }
    public set name(name: string) {
        this._name = name;
    }
    public set stateId(stateId: string){
        this._stateId = stateId;
    }
    public get id(): string {
        return this._id;
    }
    public get name(): string {
        return this._name;
    }
    public get stateId(): string {
        return this._stateId;
    }
}