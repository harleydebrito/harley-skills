import { FormBuilder, Validators } from '@angular/forms';
import { Subject, Observable, EMPTY, of, Observer } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Input, OnInit } from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { distinctUntilChanged, switchMap, tap } from 'rxjs/operators';

import { Client } from '../../services/clients.service';
import { BrazilianCity } from './../../../shared-module/models/brazilian-city.model';
import { BrazilianState } from './../../../shared-module/models/brazilian-state.model';
import { BrazilianCitiesService } from './../../../shared-module/services/brazilian-cities.service';
import { BrazilianStatesService } from './../../../shared-module/services/brazilian-states.service';
import { CustomValidators } from './../../../shared-module/utils/custom-validators';
import { CepService } from './../../../shared-module/services/cep.service';
import { BaseFormComponent } from 'src/app/shared-module/components/base-form/base-form.component';

@Component({
  selector: 'app-client-form-modal',
  templateUrl: './client-form-modal.component.html',
  styleUrls: ['./client-form-modal.component.scss'],
  preserveWhitespaces: true
})

export class ClientFormModalComponent extends BaseFormComponent implements OnInit  {
  @Input() public result: Subject<Client>;
  @Input() public client: Client;
  public imageChangedEvent: any;
  public croppedImage: string;
  public states: BrazilianState[];
  public cities: BrazilianCity[];

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder,
    private cepService: CepService,
    private stateService: BrazilianStatesService,
    private cityService: BrazilianCitiesService
  ) {
    super();
  }

  public ngOnInit(): void {
    this.buildForm();
    this.subscribeToInputs();
    this.getStates();
  }

  private buildForm(): void {
    this.form = this.formBuilder.group({
      name: [this.client ? this.client.name : null, [Validators.required]],
      cpf: [this.client ? this.client.cpf : null, [Validators.required]],
      rg: [this.client ? this.client.rg : null],
      phone: [this.client ? this.client.phone : null],
      address: this.formBuilder.group({
        cep: [this.client ? this.client.address.cep : null, [Validators.required, CustomValidators.cep]],
        city: [this.client ? this.client.address.city : null, [Validators.required]],
        complement: [this.client ? this.client.address.complement : null],
        country: [this.client ? this.client.address.country : "Brasil", [Validators.required]],
        number: [this.client ? this.client.address.number : null, [Validators.required]],
        state: [this.client ? this.client.address.state : null, [Validators.required]],
        street: [this.client ? this.client.address.street : null, [Validators.required]],
        district: [this.client ? this.client.address.district : null, [Validators.required]],
      })
    });
  }

  private subscribeToInputs(): void {
    this.subscribeToCepStatus();
    this.subscribeToStateValue();
  }

  private subscribeToCepStatus(): void {
    this.form.get('address.cep').statusChanges
      .pipe(
        distinctUntilChanged(),
        switchMap(status => status === 'VALID' ? this.cepService.search(this.form.get('address.cep').value) : of(this.disableOrEnableAddressInputs())),
      )
      .subscribe(data => data ? this.addressFill(data) : {});
  }

  private subscribeToStateValue(): void {
    this.form.get('address.state').valueChanges.pipe(
      tap((_) => this.form.get('address.cep').status === 'VALID' ? EMPTY : this.client ? EMPTY : this.form.get('address.city').reset()),
      ).subscribe((state: BrazilianState) => state ? this.getCities(state.id) : EMPTY);
  }

  private async getStates(): Promise<void> {
    this.states = await this.stateService.getStates();
    if (this.client) {
      let state = await this.stateService.getByInitial(this.client.address.state);
      this.form.patchValue({
        address: { state }
      });
    }
  }

  private async getCities(stateId: string): Promise<void> {
    this.cities = await this.cityService.getCities(stateId);
  }

  private async addressFill(address: any): Promise<void> {
    let state: BrazilianState = await this.stateService.getByInitial(address.uf);
    this.patchAddressFields(address, state);
    this.disableOrEnableAddressInputs(address);
  }

  private patchAddressFields(fromService?: any, fromLocal?: BrazilianState) {
    this.form.patchValue({
      address: {
        street: fromService.logradouro,
        complement: fromService.complemento,
        district: fromService.bairro,
        state: fromLocal,
        city: fromService.localidade
      }
    });
  }

  private disableOrEnableAddressInputs(address?: any, enable?: boolean): void {
    address?.logradouro ? this.form.get('address.street').disable() : this.enableAndResetField('address.street');
    address?.complemento ? this.form.get('address.complement').disable() : this.enableAndResetField('address.complement');
    address?.bairro ? this.form.get('address.district').disable() : this.enableAndResetField('address.district');
    address?.localidade ? this.form.get('address.city').disable() : this.enableAndResetField('address.city');
    address?.localidade ? this.form.get('address.state').disable() : this.enableAndResetField('address.state');
    this.enableAndResetField('address.number');
  }

  private enableAndResetField(field: string): void {
    this.form.get(field).enable();
    this.form.get(field).reset();
  }

  public onSelectImage(event: any): void {
    this.imageChangedEvent = event;
    document.getElementById('profilePhotoLabel').innerHTML = event.srcElement.files[0].name;
  }

  public imageCropped(event: ImageCroppedEvent): void {
    this.croppedImage = event.base64;
  }

  public compareStates(state1: BrazilianState, state2: BrazilianState): boolean | BrazilianState{
    return state1 && state2 ? (state1.initials == state2.initials) : state1 && state2;
  }

  public submit(): void {
    this.form.get('address.street').enable();
    this.form.get('address.complement').enable();
    this.form.get('address.district').enable();
    this.form.get('address.city').enable();
    this.form.get('address.state').enable();
    this.result.next(this.mappedResult());
    this.activeModal.close();
  }

  private mappedResult(): Client {
    return {
      ...this.form.value,
      deleted: false,
      id: this.client ? this.client.id : null,
      photo: this.croppedImage ? this.croppedImage : this.client.photo ? true : false,
      address: {
        ...this.form.get('address').value,
        cep: this.form.get('address.cep').value.replace('-', ''),
        state: this.form.get('address.state').value['initials'],
      }
    };
  }
}
