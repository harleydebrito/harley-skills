import { AngularFirestore, AngularFirestoreCollection, DocumentSnapshot } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';

export enum Collections {
  CLIENTS = 'clients',
  USERS = 'users'
}

export enum CounterOperation {
  INCREMENT = 1,
  DECREMENT = 0
}

export interface DocumentsCounter {
  number: number;
}

@Injectable({
  providedIn: 'root'
})

export class CounterService {

  constructor(private firestore: AngularFirestore) { }

  public async getCount(collection: Collections): Promise<number | any> {
    try {
      let response = await this.firestore.collection<DocumentsCounter>('counter').doc(collection).get().toPromise();
      return response.data().number;
    } catch (error) {
      console.log(error);
      return Promise.reject(error);
    }
  }

  public async change(collection: Collections, operation: CounterOperation): Promise<number | any> {
    try {
      let count = await this.getCount(collection) as number;
      count = operation ? count + 1 : count - 1;
      await this.firestore.collection<DocumentsCounter>('counter').doc(collection).update({ number: count });
      let newCount = await this.getCount(collection) as number;
      return newCount;
    } catch (error) {
      console.log(error);
      return Promise.reject(error);
    }
  }
}
