import { Subscription, Observable, BehaviorSubject } from 'rxjs';
import { NavigationEnd, Router, Event } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class AppUrlService {
  private _routes: BehaviorSubject<string[]>;

  constructor(private router: Router) {
    this._routes = new BehaviorSubject<string[]>(router.url.split('/'));
  }

  public listenToRouterEvents(): Subscription {
    this.broadcast(this.router.url);
    return this.router.events.subscribe((events: Event) => {
      if(events instanceof NavigationEnd){
        this.broadcast(events.url);
      }
    });
  }

  public get routes(): Observable<string[]> {
    return this._routes.asObservable();
  }

  private broadcast(route: string): void {
    this._routes.next(route.split('/'));
  }
}
