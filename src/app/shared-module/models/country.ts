import { EMPTY } from 'rxjs';

export class Country{
    private _code: string;
    private _fone: string;
    private _iso: string;
    private _iso3: string;
    private _name: string;
    private _formalName: string;
    constructor(
        code?: string,
        fone?: string,
        iso?: string,
        iso3?: string,
        name?: string,
        formalName?: string
    ){
        code ? this._code = code : EMPTY;
        fone ? this._fone = fone : EMPTY;
        iso ? this._iso = iso : EMPTY;
        iso3 ? this._iso3 = iso3 : EMPTY;
        name ? this._name = name : EMPTY;
        formalName ? this._formalName = formalName : EMPTY;
    }
    public set code(code: string){
        this._code = code;
    }
    public set fone(fone: string){
        this._fone = fone;
    }
    public set iso(iso: string){
        this._iso = iso;
    }
    public set iso3(iso3: string){
        this._iso3 = iso3;
    }
    public set name(name: string){
        this._name = name;
    }
    public set formalName(formalName: string){
        this._formalName = formalName;
    }
    public get code(): string{
        return this._code;
    }
    public get fone(): string{
        return this._fone;
    }
    public get iso(): string{
        return this._iso;
    }
    public get iso3(): string{
        return this._iso3;
    }
    public get name(): string{
        return this._name;
    }
    public get formalName(): string{
        return this._formalName;
    }
}