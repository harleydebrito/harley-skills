import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientFormScreenComponent } from './client-form-screen.component';

describe('ClientFormScreenComponent', () => {
  let component: ClientFormScreenComponent;
  let fixture: ComponentFixture<ClientFormScreenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClientFormScreenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientFormScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
