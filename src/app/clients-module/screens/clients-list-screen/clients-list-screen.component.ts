import { FormControl } from '@angular/forms';
import { take, map, filter, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Client, ClientsService, ClientsServiceResponse } from './../../services/clients.service';
import { Observable, EMPTY, Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';

import { AlertTypes } from './../../../shared-module/utils/alert-types.enum';
import { Alert } from './../../../shared-module/models/alert.model';
import { ClientFormModalComponent } from './../../modals/client-form-modal/client-form-modal.component';
import { ModalService } from './../../../shared-module/services/modal.service';

@Component({
  selector: 'app-clients-list-screen',
  templateUrl: './clients-list-screen.component.html',
  styleUrls: ['./clients-list-screen.component.scss'],
  preserveWhitespaces: true
})

export class ClientsListScreenComponent implements OnInit {
  public clients$: Observable<Client[]> = new Observable();
  public clients: Client[] = new Array<Client>();
  public alerts: Alert[] = new Array<Alert>();
  public clientsCount: number;
  public page: number = 1;
  public numberOfPages: number;
  public pageSize: number = 5;
  public searchInput: FormControl = new FormControl();
  public reactiveSearchListener$: Subscription;

  constructor(
    public clientsService: ClientsService,
    private modalService: ModalService,
  ) { }

  public ngOnInit(): void {
    //Página costuma rolar um pouco para baixo, para impedir isso, faço o scroll ser 0.
    document.body.scrollTop = document.documentElement.scrollTop = 0;
    this.loadClients();
    this.reactiveSearchListener$ = this.listenToReactiveSearch();
  }

  public listenToReactiveSearch(): Subscription {
    return this.searchInput.valueChanges.pipe(
      map(value => value.trim()),
      debounceTime(400),
      distinctUntilChanged(),
    ).subscribe((_) => this.loadClients());
  }

  public listenToPageChange(page: number): void {
    document.body.scrollTop = document.documentElement.scrollTop = document.getElementById("scrollTarget").offsetTop - 25;
    if (page == this.page - 1) {
      this.loadClients({ previous: true });
    } else if (page == this.page + 1) {
      this.loadClients({ next: true });
    }
  }

  public loadClients({ closeAlerts = false, size, next, previous }:{ closeAlerts?: boolean, size?: number, next?: boolean, previous?: boolean } = {}): void {
    next || previous ? EMPTY : this.page = 1;
    closeAlerts ? this.closeAlert() : EMPTY;
    this.clientsService.load(
      { size: size ? size : this.pageSize,
        last: next ? this.clients[this.clients.length - 1] : null,
        first: previous ? this.clients[0] : null,
        name: this.searchInput.value ? this.searchInput.value : null
      }).then(
      (response: ClientsServiceResponse) => {
        this.clients = response.payload;
        this.clientsCount = this.searchInput.value ? this.clients.length : response.count;
        this.numberOfPages = this.clientsCount/this.pageSize;
      },
      (response: ClientsServiceResponse) => {
        this.showAlert(response);
      }
    );
  }

  public onNewClientPressed(): void {
    this.closeAlert();
    const modalResult$: Observable<Client> = this.modalService.showModalWithExpectedResult(ClientFormModalComponent).asObservable();
    modalResult$.pipe(take(1)).subscribe(
      (result: Client) => {
        this.clientsService.save(result).then(
          (response: ClientsServiceResponse) => {
            this.showAlert(response);
            this.loadClients();
          },
          (reason: ClientsServiceResponse) => {
            this.showAlert(reason);
          }
        );
      }
    );
  }

  public onDeletePressed(client: Client): void {
    const modalResult$: Observable<boolean> = this.modalService.showConfirm({ title: 'Excluir cliente', message: `Deseja excluir o cliente ${client.name}?` });
    modalResult$.pipe(take(1)).subscribe(
      (result: boolean) => {
        if(result){
          this.clientsService.delete(client.id).then(
            (response: ClientsServiceResponse) => {
              this.closeAlert();
              this.showAlert(response);
              this.loadClients();
            }
          );
        }
      }
    );
  }

  public onEditPressed(client: Client): void {
    this.closeAlert();
    const modalResult$: Observable<Client> = this.modalService.showModalWithExpectedResult(ClientFormModalComponent, client).asObservable();
    modalResult$.pipe(take(1)).subscribe(
      (result: Client) => {
        this.clientsService.save(result).then(
          (response: ClientsServiceResponse) => {
            this.showAlert(response);
            this.loadClients();
          },
          (reason: ClientsServiceResponse) => {
            this.showAlert(reason);
          }
        );
      }
    );
  }

  private showAlert(response: ClientsServiceResponse): void {
    if(response.result){
      this.alerts.push(new Alert(AlertTypes.SUCCESS, response.message));
    }else{
      this.alerts.push(new Alert(AlertTypes.DANGER, response.message));
    }
  }

  public closeAlert(index?: number): void {
    index? this.alerts.splice(index, 1) : this.alerts = new Array();
  }
}
