import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeScreenComponent } from './screens/home-screen/home-screen.component';
import { SharedModule } from './../shared-module/shared.module';

@NgModule({
  declarations: [
    HomeScreenComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    NgbModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
