import { CustomValidators } from './../../../shared-module/utils/custom-validators';
import { ModalService } from './../../../shared-module/services/modal.service';
import { FirebaseAuthService } from '../../services/firebase-auth.service';
import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { BaseFormComponent } from 'src/app/shared-module/components/base-form/base-form.component';
import { AuthenticationErrorMsgService } from '../../services/authentication-error-msg.service';

@Component({
  selector: 'app-register-screen',
  templateUrl: './register-screen.component.html',
  styleUrls: ['./register-screen.component.scss']
})
export class RegisterScreenComponent extends BaseFormComponent implements OnInit, OnDestroy {
  public firebaseAuthService$: Subscription;
  
  constructor(
    public firebaseAuthService: FirebaseAuthService,
    private formBuilder: FormBuilder,
    private modalService: ModalService,
    private router: Router,
    private loginErrorMsgService: AuthenticationErrorMsgService
  ) {
    super();
  }

  public ngOnInit(): void {
    this.buildRegisterForm();
    this.listenToFirebaseAuthService();
  }

  public ngOnDestroy(): void {
    this.firebaseAuthService$.unsubscribe();
  }

  private buildRegisterForm(): void {
    this.form = this.formBuilder.group({
      name: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]],
      passwordConfirm: [null, [CustomValidators.equalsTo('password')]]
    });
  }

  private listenToFirebaseAuthService(): void {
    this.firebaseAuthService$ = this.firebaseAuthService.subject.asObservable().subscribe(
      response => this.firebaseAuthServiceResponse(response)
    );
  }

  private firebaseAuthServiceResponse(res: any): void {
    if(res.success) {
      this.firebaseAuthSuccess(res.content);
    }else{
      this.firebaseAuthFailure(res.content);
    }
  }

  private firebaseAuthSuccess(user: firebase.default.User): void {
    this.router.navigate(['login'], { queryParams: { redirect: true } });
  }

  private firebaseAuthFailure(code: number): void {
    this.modalService.showDangerAlert(code.toString());
  }

  public submit(): void {
    this.firebaseAuthService.signUp(this.form.value);
  }
}
