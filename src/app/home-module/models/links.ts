import { EMPTY } from 'rxjs';

export class Link {
    private _title: string;
    private _route: string;

    constructor(title?: string, route?: string){
        title ? this._title = title : EMPTY;
        route ? this._route = route : EMPTY;
    }

    public set title(title: string) {
        this._title = title;
    }

    public set route(route: string) {
        this._route = route;
    }

    public get title(): string {
        return this._title;
    }

    public get route(): string {
        return this._route;
    }
}