import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class CepService {

  constructor(private http: HttpClient) { }

  public search(cep: string): Observable<any> {
    cep = cep.replace(/\D/g, '');

    if(cep != "") {
      var cepPattern = /^[0-9]{8}$/;

      if(cepPattern.test(cep)) {
        return this.http.get(`//viacep.com.br/ws/${cep}/json`);
      }
    }

    return of({});
  }
}
